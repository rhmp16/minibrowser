package com.thm.reh.minibrowser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

public class UrlActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.url_layout);
    }

    public void onClickOpenURL(View v)
    {
        //Referenz auf den Edittext + hole den Text
        EditText urlText = findViewById(R.id.urlEditText);
        String url = urlText.getText().toString();

        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra(getString(R.string.EXTRA_URL), url);
        startActivity(intent);
    }
}
